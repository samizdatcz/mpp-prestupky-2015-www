brequire! {
  fs
  async
  diacritics
}
require! "csv-parse":csvParse

file = "praha-rychlost"
targetDir = "praha-rychlost"

stream = fs.createReadStream "#__dirname/../data/#file.csv"
reader = csvParse(delimiter: ',')
stream.pipe reader

isRychlost = 'rychlost' is targetDir.split '-' .1

i = 0
out = {}
finish = (cb) ->
  files = for id, data of out
    {id, data}
  console.log "Saving #{files.length} files"
  saved = 0
  <~ async.eachLimit files, 20, ({id, data}, cb) ->
    saved += data.split "\n" .length
    <~ fs.writeFile "#__dirname/../data/processed/#targetDir/tiles/#{id}.tsv", data
    process.nextTick cb
  console.log "Saved #saved lines"
  cb!
lines = 0
typIndices = {}
typFulls = {}
toDouble = ->
  if it.length == 1
    "0" + it
  else
    it

currentTypIndex = 0
reader.on \data (line) ->
  if 'praha-rychlost' == file
    [_, _, _, typ, _, _, spachano, _, _, y, x] = line
    return if y == "y"
    [date, time] = spachano.split " "
    [d, m, yr] = date.split "."
    [h] = time.split ":"
    spachano = "#{yr}#{toDouble m}#{toDouble d}#{toDouble h}"
  else if 'mp' == file
    [x, y, spachano, _, typ] = line
    spachano .= replace /[^0-9]/g ''
  else if 'praha-odtahy' == file
    [x, y, _, typ, ..., spachano, _] = line
    spachano .= replace /[^0-9]/g ''
  else if 'brno-prestupky-parkovani' == file
    [x, y, spachano,, _, _, typ] = line
    return if not spachano
    spachano .= replace /[^0-9]/g ''
  else
    [x, y, spachano, time, addr] = line
    typ = "Neznámý"
    [d, m, yr] = spachano.split "."
    [h] = time.split ":"
    spachano = "#{yr}#{m}#{d}#{h}"


  return if y == 'y'
  x = parseFloat x
  # x -= 0.0011
  y = parseFloat y
  # y -= 0.00074
  return unless x and y
  x .= toFixed 5
  y .= toFixed 5
  xIndex = (Math.floor x / 0.01)
  yIndex = (Math.floor y / 0.005)
  spachano = spachano.substr 2, 8
  originalTyp = typ
  # typ = diacritics.remove typ
  typ .= toLowerCase!
  typ .= replace /[^a-z0-9]/gi ''
  typ .= replace /s/g 'z'
  typId = if typIndices[typ]
    that
  else
    currentTypIndex++
    i = currentTypIndex
    typIndices[typ] = i
    typFulls[typ] = originalTyp
    i
  id = "#{xIndex}-#{yIndex}"
  lines++
  out[id] ?= 'typ\tx\ty\tspachano'
  out[id] += "\n#typId\t#x\t#y\t#spachano"

<~ reader.on \end
console.log "Found #lines records"
typy = ["typy"]
for typ, index of typIndices
  typy[index] = typFulls[typ]
fs.writeFile "#__dirname/../data/processed/#targetDir/typy.tsv", typy.join "\n"
<~ finish!
